#!/bin/bash
#
# 变量配置文件
# 作者: Grove Zhao
# 邮箱: grove.zhao@qq.com

############颜色变量############
declare -r RED='\033[0;31m'
declare -r GREEN='\033[0;32m'
declare -r YELLOW='\033[0;33m'
declare -r BLUE='\033[0;34m'
declare -r PLAIN='\033[0m'

##########公共目录配置##########
export SRC_DIR="/usr/local/src"
export INSTALL_DIR="/service"
export LOG_DIR="/var/log/scricp"
export SCRICP_DIR="/root/script/lib"
export PROFILE_DIR="/etc/profile.d"


#############################main 变量配置#############################
####脚本文件配置####
# Oracle JDK 脚本文件
export ORACLE_JDK_INSTALL="${SCRICP_DIR}/install_oracle_jdk.sh"
export ORACLE_JDK_UNINSTALL="${SCRICP_DIR}/uninstall_oracle_jdk.sh"
# Open JDK 脚本文件
export OPEN_JDK_INSTALL="${SCRICP_DIR}/install_open_jdk.sh"
export OPEN_JDK_UNINSTALL="${SCRICP_DIR}/uninstall_open_jdk.sh"
# Maven 脚本文件
export MAVEN_INSTALL="${SCRICP_DIR}/install_maven.sh"
export MAVEN_UNINSTALL="${SCRICP_DIR}/uninstall_maven.sh"
# MySQL 脚本文件
export MYSQL_INSTALL="${SCRICP_DIR}/install_mysql.sh"
export MYSQL_UNINSTALL="${SCRICP_DIR}/uninstall_mysql.sh"

####日志文件配置####
export LOG_FILE="${LOG_DIR}/main.log"
# Oracle JDK 日志文件 
export ORACLE_JDK_INSTALL_LOG="${LOG_DIR}/oracle_jdk_install.log"
export ORACLE_JDK_UNINSTALL_LOG="${LOG_DIR}/oracle_jdk_uninstall.log"
# Open JDK 日志文件
export OPEN_JDK_INSTALL_LOG="${LOG_DIR}/open_jdk_install.log"
export OPEN_JDK_UNINSTALL_LOG="${LOG_DIR}/open_jdk_uninstall.log"
# Maven 日志文件
export MAVEN_INSTALL_LOG="${LOG_DIR}/maven_install.log"
export MAVEN_UNINSTALL_LOG="${LOG_DIR}/maven_uninstall.log"
# MySQL 日志文件
export MYSQL_INSTALL_LOG="${LOG_DIR}/mysql_install.log"
export MYSQL_UNINSTALL_LOG="${LOG_DIR}/mysql_uninstall.log"

#############################Oracle JDK 变量配置#############################
export ORACLE_JDK_VERSION="11.0.24"
export ORACLE_JDK_SYMLINK="/usr/local/oracle_jdk"
export ORACLE_JDK_URL="https://d.injdk.cn/d/download/oraclejdk/11/"
export ORACLE_JDK_FILE="jdk-11.0.24_linux-x64_bin.tar.gz"
export ORACLE_JDK_PROFILE="${PROFILE_DIR}/oracle_jdk.sh"


#############################Open JDK 变量配置#############################
export OPEN_JDK_VERSION="11.0.2"
export OPEN_JDK_SYMLINK="/usr/local/open_jdk"
export OPEN_JDK_URL="https://download.java.net/java/GA/jdk11/9/GPL/"
export OPEN_JDK_FILE="openjdk-11.0.2_linux-x64_bin.tar.gz"
export OPEN_JDK_PROFILE="${PROFILE_DIR}/open_jdk.sh"

#############################Maven 变量配置#############################
export MAVEN_VERSION="3.8.8"
export MAVEN_SYMLINK="/usr/local/maven"
export MAVEN_URL="https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries"
export MAVEN_FILE="apache-maven-${MAVEN_VERSION}-bin.tar.gz"
export MAVEN_PROFILE="${PROFILE_DIR}/maven.sh"
export MAVEN_SETTINGS_FILE="${MAVEN_SYMLINK}/conf/settings.xml"

#############################MySQL 变量配置#############################
export MYSQL_VERSION="8.0.36"
export MYSQL_DATA_DIR="/data/mysql"
export MYSQL_SYMLINK="/usr/local/mysql"
export MYSQL_URL="https://downloads.mysql.com/archives/get/p/23/file/"
export MYSQL_FILE="${MYSQL_VERSION}-linux-glibc2.17-x86_64.tar.xz"
export MYSQL_ROOT_PASSWORD="Password@123"

#############################PYTHON 变量配置#############################
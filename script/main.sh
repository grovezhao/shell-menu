#!/bin/bash
#
# 支持: Ubuntu、RedHat、CentOS、Rocky、AnolisOS
# 作者: Grove Zhao
# 邮箱: grove.zhao@qq.com
# 版本: v1

# 变量配置文件
source /root/script/config/variable_config.sh

# 创建日志目录
mkdir -p "${LOG_DIR}"

# 日志和提示函数
log() {
    local msg=$1
    local color=$2
    local timestamp
    timestamp=$(date '+%Y-%m-%d %H:%M:%S')

    # 输出到日志文件（带颜色）
    echo -e "${color}${timestamp} ${msg}${PLAIN}" >> "$LOG_FILE"

    # 输出到控制台（带颜色）
    echo -e "${color}${timestamp} ${msg}${PLAIN}"
}

# 简化的日志函数
error()   { log "$1" "$RED" >&2; }
warn()    { log "$1" "$YELLOW"; }
success() { log "$1" "$GREEN"; }
info()    { log "$1" "$BLUE"; }

# 暂停函数
function pause() {
    local message="${1:-按任意键继续...}" # 如果没有提供消息，使用默认消息
    info "${message}"
    read -n 1 -s
    echo # 添加换行，使输出更整洁
}

# 错误处理函数
output_error() {
    local message=$1
    error "${message}"
    exit 1
}

# 权限判定
function permission_judgment() {
    if ((UID != 0)); then
        output_error "权限不足，请使用 Root 用户运行本脚本"
    fi
}

# 显示菜单函数
function run_start() {
    if [ -z "${CLEAN_SCREEN}" ]; then
        [ -z "${SOURCE}" ] && clear
    elif [ "${CLEAN_SCREEN}" == "true" ]; then
        clear
    fi
    printf '+-----------------------------------+\n'
    printf "| \033[0;1;35;95m⡇\033[0m  \033[0;1;33;93m⠄\033[0m \033[0;1;32;92m⣀⡀\033[0m \033[0;1;36;96m⡀\033[0;1;34;94m⢀\033[0m \033[0;1;35;95m⡀⢀\033[0m \033[0;1;31;91m⡷\033[0;1;33;93m⢾\033[0m \033[0;1;32;92m⠄\033[0m \033[0;1;36;96m⡀⣀\033[0m \033[0;1;34;94m⡀\033[0;1;35;95m⣀\033[0m \033[0;1;31;91m⢀⡀\033[0m \033[0;1;33;93m⡀\033[0;1;32;92m⣀\033[0m \033[0;1;36;96m⢀⣀\033[0m |\n"
    printf "| \033[0;1;31;91m⠧\033[0;1;33;93m⠤\033[0m \033[0;1;32;92m⠇\033[0m \033[0;1;36;96m⠇⠸\033[0m \033[0;1;34;94m⠣\033[0;1;35;95m⠼\033[0m \033[0;1;31;91m⠜⠣\033[0m \033[0;1;33;93m⠇\033[0;1;32;92m⠸\033[0m \033[0;1;36;96m⠇\033[0m \033[0;1;34;94m⠏\033[0m  \033[0;1;35;95m⠏\033[0m  \033[0;1;33;93m⠣⠜\033[0m \033[0;1;32;92m⠏\033[0m  \033[0;1;34;94m⠭⠕\033[0m |\n"
    printf '+-----------------------------------+\n'
    printf '欢迎使用 GNU/Linux 脚本\n'
}

# 标题函数
function print_title() {
    local system_name
    local arch
    local date_time
    local time_zone
    system_name="${SYSTEM_PRETTY_NAME:-"$(grep PRETTY_NAME /etc/os-release | cut -d'"' -f2)"}"
    arch="$(uname -m)"
    date_time="$(date "+%Y-%m-%d %H:%M")"
    time_zone="$(timedatectl status 2>/dev/null | grep "Time zone" | awk -F ':' '{print$2}' | awk '{print$1}')"

    printf '\n'
    printf "运行环境 ${BLUE}%s %s${PLAIN}\n" "${system_name}" "${arch}"
    printf "系统时间 ${BLUE}%s %s${PLAIN}\n" "${date_time}" "${time_zone}"
}

# 布尔选择函数
function interactive_select_boolean() {
    local message="$1"
    local selected=0
    local key

    trap 'tput cnorm; tput rmcup; exit' INT TERM

    tput sc
    tput civis

    while true; do
        tput rc
        printf "╭─ %s\n│\n╰─ %s\n" \
            "${message}" \
            "$([ $selected -eq 0 ] && echo -e "\033[32m●\033[0m 是\033[2m / ○ 否\033[0m" || echo -e "\033[2m○ 是 / \033[0m\033[32m●\033[0m 否")"

        IFS= read -rsn1 key
        [[ $key == $'\x1b' ]] && read -rsn2 key

        case "$key" in
        '[D') ((selected > 0)) && ((selected--)) ;;
        '[C') ((selected < 1)) && ((selected++)) ;;
        '') break ;;
        esac
    done

    tput cnorm
    _SELECT_RESULT=$([ "$selected" -eq 0 ] && echo "true" || echo "false")
}

# 菜单选择函数
function interactive_select_mirror() {
    local -a options=("$@")
    local message="${options[${#options[@]} - 1]}"
    unset "options[${#options[@]}-1]"
    local selected=0
    local start=0
    local page_size=$(($(tput lines) - 3))
    local key

    trap 'tput cnorm; tput rmcup; exit' INT TERM

    tput smcup
    tput civis

    while true; do
        tput clear
        printf "%s\n" "${message}"

        local end=$((start + page_size - 1))
        [[ $end -ge ${#options[@]} ]] && end=$((${#options[@]} - 1))

        for ((i = start; i <= end; i++)); do
            if ((i == selected)); then
                printf "\e[34;4m➤ %s\e[0m\n" "${options[$i]%@*}"
            else
                printf "  %s\n" "${options[$i]%@*}"
            fi
        done

        IFS= read -rsn1 key
        [[ $key == $'\x1b' ]] && read -rsn2 key

        case "$key" in
        '[A')
            if ((selected > 0)); then
                ((selected--))
                ((selected < start)) && ((start--))
            fi
            ;;
        '[B')
            if ((selected < ${#options[@]} - 1)); then
                ((selected++))
                ((selected >= start + page_size)) && ((start++))
            fi
            ;;
        '')
            tput rmcup
            break
            ;;
        esac
    done

    tput cnorm
    _SELECT_RESULT="${options[$selected]}"
}

# 主菜单选项
function define_menu_options() {
    MENU_OPTIONS=(
        "服务管理@service_management"
        "退出程序@exit_script"
        "请选择要执行的操作："
    )
}

# 安装服务子菜单选项
function define_install_service_options() {
    INSTALL_OPTIONS=(
        "Oracle JDK 管理@manage_OracleJDK"
        "Open JDK 管理@manage_OpenJDK"
        "Maven 管理@manage_Maven"
        "MySQL 管理@manage_MySQL"
        "返回上级菜单@back"
        "请选择要安装的服务："
    )
}

# 安装服务子菜单选项函数
function service_manager() {
    while true; do
        clear
        print_title
        define_install_service_options
        interactive_select_mirror "${INSTALL_OPTIONS[@]}"
        local selection="${_SELECT_RESULT#*@}"

        case "$selection" in
        manage_OracleJDK)
            manage_oracle_jdk_menu
            ;;
        manage_OpenJDK)
            manage_open_jdk_menu
            ;;
        manage_Maven)
            manage_maven_menu
            ;;
        manage_MySQL)
            manage_mysql_menu
            ;;
        back)
            return
            ;;
        esac
    done
}

# Oracle JDK 管理菜单选项
function define_oracle_jdk_options() {
    ORACLE_JDK_OPTIONS=(
        "安装 Oracle JDK@installation_oracle_jdk"
        "卸载 Oracle JDK@uninstall_oracle_jdk"
        "返回上级菜单@back"
        "请选择要执行的操作："
    )
}

function manage_oracle_jdk_menu() {
    while true; do
        clear
        print_title
        define_oracle_jdk_options
        interactive_select_mirror "${ORACLE_JDK_OPTIONS[@]}"
        local selection="${_SELECT_RESULT#*@}"

        case "$selection" in
        installation_oracle_jdk)
            install_oracle_jdk
            pause "按任意键返回菜单..."
            ;;
        uninstall_oracle_jdk)
            unload_oracle_jdk
            pause "按任意键返回菜单..."
            ;;
        back)
            return
            ;;
        esac
    done
}

# Open JDK 管理菜单选项
function define_open_jdk_options() {
    OPEN_JDK_OPTIONS=(
        "安装 Open JDK@installation_open_jdk"
        "卸载 Open JDK@uninstall_open_jdk"
        "返回上级菜单@back"
        "请选择要执行的操作："
    )
}

function manage_open_jdk_menu() {
    while true; do
        clear
        print_title
        define_open_jdk_options
        interactive_select_mirror "${OPEN_JDK_OPTIONS[@]}"
        local selection="${_SELECT_RESULT#*@}"

        case "$selection" in
        installation_open_jdk)
            install_open_jdk
            pause "按任意键返回菜单..."
            ;;
        uninstall_open_jdk)
            unload_open_jdk
            pause "按任意键返回菜单..."
            ;;
        back)
            return
            ;;
        esac
    done
}

# Maven 管理菜单选项
function define_maven_options() {
    MAVEN_OPTIONS=(
        "安装 Maven@installation_maven"
        "卸载 Maven@uninstall_maven"
        "返回上级菜单@back"
        "请选择要执行的操作："
    )
}

function manage_maven_menu() {
    while true; do
        clear
        print_title
        define_maven_options
        interactive_select_mirror "${MAVEN_OPTIONS[@]}"
        local selection="${_SELECT_RESULT#*@}"

        case "$selection" in
        installation_maven)
            install_maven
            pause "按任意键返回菜单..."
            ;;
        uninstall_maven)
            unload_maven
            pause "按任意键返回菜单..."
            ;;
        back)
            return
            ;;
        esac
    done
}

# MySQL 管理菜单选项
function define_mysql_options() {
    MYSQL_OPTIONS=(
        "安装 MySQL@installation_mysql"
        "卸载 MySQL@uninstall_mysql"
        "返回上级菜单@back"
        "请选择要执行的操作："
    )
}

function manage_mysql_menu() {
    while true; do
        clear
        print_title
        define_mysql_options
        interactive_select_mirror "${MYSQL_OPTIONS[@]}"
        local selection="${_SELECT_RESULT#*@}"

        case "$selection" in
        installation_mysql)
            install_mysql
            pause "按任意键返回菜单..."
            ;;
        uninstall_mysql)
            unload_mysql
            pause "按任意键返回菜单..."
            ;;
        back)
            return
            ;;
        esac
    done
}

# 安装 Oracle JDK
function install_oracle_jdk() {
    clear
    info "开始安装 Oracle JDK..."

    if [ ! -f "$ORACLE_JDK_INSTALL" ]; then
        error "Oracle JDK 安装脚本不存在: $ORACLE_JDK_INSTALL"
        return 1
    fi

    if [ ! -x "$ORACLE_JDK_INSTALL" ]; then
        warn "Oracle JDK 安装脚本没有执行权限，尝试添加执行权限..."
        if ! chmod +x "$ORACLE_JDK_INSTALL"; then
            error "无法为安装脚本添加执行权限"
            return 1
        fi
        success "成功添加执行权限"
    fi

    info "安装脚本: $ORACLE_JDK_INSTALL"
    info "安装日志: $ORACLE_JDK_INSTALL_LOG"
    interactive_select_boolean "是否继续安装 Oracle JDK? [是/否]"
    if [ "$_SELECT_RESULT" != "true" ]; then
        info "已取消安装"
        return 0
    fi

    info "开始执行 Oracle JDK 安装脚本......"
    if bash "$ORACLE_JDK_INSTALL"; then
        # 验证 Java 安装
        info "正在验证 Java 安装......"
        if ! command -v java &>/dev/null; then
            error "Java 命令不可用"
            return 1
        fi

        # 检查 Java 版本
        local java_version
        java_version=$(java -version 2>&1 | grep -i version | awk -F '"' '{print $2}' | awk -F '_' '{print $1}')
        if [ $? -eq 0 ] && [ ! -z "$java_version" ]; then
            success "Java 版本: $java_version"
            success "Oracle JDK 安装成功"
        else
            error "Oracle JDK 安装失败"
            return 1
        fi
    else
        error "Oracle JDK 脚本执行失败"
        return 1
    fi
}

# 卸载 Oracle JDK 
function unload_oracle_jdk() {
    clear
    info "开始卸载 Oracle JDK..."

    # 检查安装脚本是否存在
    if [ ! -f "$ORACLE_JDK_UNINSTALL" ]; then
        error "Oracle JDK 安装脚本不存在: $ORACLE_JDK_UNINSTALL"
        return 1
    fi

    if [ ! -x "$ORACLE_JDK_UNINSTALL" ]; then
        warn "Oracle JDK 安装脚本没有执行权限，尝试添加执行权限..."
        if ! chmod +x "$ORACLE_JDK_UNINSTALL"; then
            error "无法为安装脚本添加执行权限"
            return 1
        fi
        success "成功添加执行权限"
    fi

    info "将使用卸载脚本: $ORACLE_JDK_UNINSTALL"
    info "卸载日志将保存到: $ORACLE_JDK_UNINSTALL_LOG"
    interactive_select_boolean "是否卸载 Oracle JDK? [是/否]"
    if [ "$_SELECT_RESULT" != "true" ]; then
        info "已取消安装"
        return 0
    fi

    info "正在执行 Oracle JDK 卸载脚本..."
    if bash "$ORACLE_JDK_UNINSTALL"; then
        success "Oracle JDK 卸载成功"
    else
        error "Oracle JDK 卸载失败"
        return 1
    fi

}

# 安装 Open JDK 
function install_open_jdk() {
    clear
    info "开始安装 Open JDK..."

    if [ ! -f "$OPEN_JDK_INSTALL" ]; then
        error "Open JDK 安装脚本不存在: $OPEN_JDK_INSTALL"
        return 1
    fi

    if [ ! -x "$OPEN_JDK_INSTALL" ]; then
        warn "Open JDK 安装脚本没有执行权限，尝试添加执行权限..."
        if ! chmod +x "$OPEN_JDK_INSTALL"; then
            error "无法为安装脚本添加执行权限"
            return 1
        fi
        success "成功添加执行权限"
    fi

    info "将使用安装脚本: $OPEN_JDK_INSTALL"
    info "安装日志将保存到: $OPEN_JDK_INSTALL_LOG"
    interactive_select_boolean "是否继续安装 Open JDK? [是/否]"
    if [ "$_SELECT_RESULT" != "true" ]; then
        info "已取消安装"
        return 0
    fi

    info "正在执行 Open JDK 安装脚本..."
    if bash "$OPEN_JDK_INSTALL"; then
        # 验证 Java 安装
        info "正在验证 Java 安装..."
        if ! command -v java &>/dev/null; then
            error "Java 命令不可用"
            return 1
        fi

        # 检查 Java 版本
        local java_version
        java_version=$(java -version 2>&1 | grep -i version | awk -F '"' '{print $2}')
        if [ $? -eq 0 ] && [ ! -z "$java_version" ]; then
            success "Java 版本: $java_version"
            success "Open JDK 安装成功"
        else
            error "Open JDK 安装失败"
            return 1
        fi
    else
        error "Open JDK 脚本执行失败"
        return 1
    fi
}

# 卸载 Open JDK 
function unload_open_jdk() {
    clear

    # 检查卸载脚本是否存在
    if [ ! -f "$OPEN_JDK_UNINSTALL" ]; then
        error "Open JDK 卸载脚本不存在: $OPEN_JDK_UNINSTALL"
        return 1
    fi

    if [ ! -x "$OPEN_JDK_UNINSTALL" ]; then
        warn "Open JDK 卸载脚本没有执行权限，尝试添加执行权限..."
        if ! chmod +x "$OPEN_JDK_UNINSTALL"; then
            error "无法为卸载脚本添加执行权限"
            return 1
        fi
        success "成功添加执行权限"
    fi

    info "将使用卸载脚本: $OPEN_JDK_UNINSTALL"
    info "卸载日志将保存到: $OPEN_JDK_UNINSTALL_LOG"
    interactive_select_boolean "是否卸载 Open JDK? [是/否]"
    if [ "$_SELECT_RESULT" != "true" ]; then
        info "已取消卸载"
        return 0
    fi

    info "正在执行 Open JDK 卸载脚本..."
    if bash "$OPEN_JDK_UNINSTALL"; then
        success "Open JDK 卸载成功"
    else
        error "Open JDK 卸载失败"
        return 1
    fi
}

# 安装 Maven 
function install_maven() {
    clear
    info "开始安装 Maven..."

    if [ ! -f "$MAVEN_INSTALL" ]; then
        error "Maven 安装脚本不存在: $MAVEN_INSTALL"
        return 1
    fi

    if [ ! -x "$MAVEN_INSTALL" ]; then
        warn "Maven 安装脚本没有执行权限，尝试添加执行权限..."
        if ! chmod +x "$MAVEN_INSTALL"; then
            error "无法为安装脚本添加执行权限"
            return 1
        fi
        success "成功添加执行权限"
    fi

    info "将使用安装脚本: $MAVEN_INSTALL"
    info "安装日志将保存到: $MAVEN_INSTALL_LOG"
    interactive_select_boolean "是否继续安装 Maven? [是/否]"
    if [ "$_SELECT_RESULT" != "true" ]; then
        info "已取消安装"
        return 0
    fi

    info "正在执行 Maven 安装脚本..."
    if bash "$MAVEN_INSTALL"; then
        # 验证 Maven 安装
        info "正在验证 Maven 安装..."
        if ! command -v mvn &>/dev/null; then
            error "Maven 命令不可用"
            return 1
        fi

        # 检查 Maven 版本
        local maven_version
        maven_version=$(mvn -version 2>&1 | grep "Apache Maven" | awk '{print $3}')
        if [ $? -eq 0 ] && [ ! -z "$maven_version" ]; then
            success "Maven 版本: $maven_version"
            success "Maven 安装成功"
        else
            error "Maven 安装失败"
            return 1
        fi
    else
        error "Maven 脚本执行失败"
        return 1
    fi
}

# 卸载 Maven 
function unload_maven() {
    clear
    info "开始卸载 Maven..."

    # 检查卸载脚本是否存在
    if [ ! -f "$MAVEN_UNINSTALL" ]; then
        error "Maven 卸载脚本不存在: $MAVEN_UNINSTALL"
        return 1
    fi

    if [ ! -x "$MAVEN_UNINSTALL" ]; then
        warn "Maven 卸载脚本没有执行权限，尝试添加执行权限..."
        if ! chmod +x "$MAVEN_UNINSTALL"; then
            error "无法为卸载脚本添加执行权限"
            return 1
        fi
        success "成功添加执行权限"
    fi

    info "将使用卸载脚本: $MAVEN_UNINSTALL"
    info "卸载日志将保存到: $MAVEN_UNINSTALL_LOG"
    interactive_select_boolean "是否卸载 Maven? [是/否]"
    if [ "$_SELECT_RESULT" != "true" ]; then
        info "已取消卸载"
        return 0
    fi

    info "正在执行 Maven 卸载脚本..."
    if bash "$MAVEN_UNINSTALL"; then
        success "Maven 卸载成功"
    else
        error "Maven 卸载失败"
        return 1
    fi
}

# 安装 MySQL 
function install_mysql() {
    clear
}

# 卸载 MySQL
function unload_mysql() {
    clear
}


# 主菜单函数
function main() {
    permission_judgment
    detect_package_manager
    while true; do
        run_start
        print_title
        define_menu_options
        interactive_select_mirror "${MENU_OPTIONS[@]}"
        local selection="${_SELECT_RESULT#*@}"

        case "$selection" in
        service_management)
            service_manager
            ;;
        set_system)
            set_system
            ;;
        set_ubuntu_system)
            set_ubuntu_system
            ;;
        exit_script)
            exit 0
            ;;
        esac
    done
}

main
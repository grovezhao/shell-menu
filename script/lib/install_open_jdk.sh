#!/bin/bash
#
# Open JDK 自动安装脚本
# 作者: Grove Zhao
# 邮箱: grove.zhao@qq.com

# 严格模式
set -euo pipefail
IFS=$'\n\t'
trap 'error "脚本执行失败: 行号 $LINENO"' ERR

# 变量配置文件
source /root/script/config/variable_config.sh

# 日志和提示函数
log() {
    local msg=$1
    local color=$2
    local timestamp
    timestamp=$(date '+%Y-%m-%d %H:%M:%S')

    # 输出到控制台（带颜色）
    #echo -e "${color}${timestamp} ${msg}${PLAIN}"

    # 输出到日志文件（带颜色）
    echo -e "${color}${timestamp} ${msg}${PLAIN}" >> "$OPEN_JDK_INSTALL_LOG"
}

# 简化的日志函数
error()   { log "$1" "$RED" >&2; }
warn()    { log "$1" "$YELLOW"; }
success() { log "$1" "$GREEN"; }
info()    { log "$1" "$BLUE"; }

# 检查运行权限
check_root() {
    if [ "$(id -u)" != "0" ]; then
        error "此脚本需要root权限运行"
        exit 1
    fi
    success "权限检查通过"
}

# 创建安装目录
create_install_dir() {
    if [ ! -d "${INSTALL_DIR}" ]; then
        mkdir -p "${INSTALL_DIR}" || {
            error "创建目录 ${INSTALL_DIR} 失败"
            exit 1
        }
        success "安装目录创建完成"
    fi
}

# 下载 Open JDK 二进制安装包
download_jdk() {
    cd "${SRC_DIR}" || {
        error "无法进入目录 ${SRC_DIR}"
        exit 1
    }

    if [ -f "${OPEN_JDK_FILE}" ]; then
        info "${OPEN_JDK_FILE} 已存在"
        return 0
    fi

    info "开始下载 Open JDK 安装包"

    # 验证下载URL是否可访问
    if ! curl --connect-timeout 10 -sI "${OPEN_JDK_URL}/${OPEN_JDK_FILE}" &>/dev/null; then
        error "Open JDK 下载地址不可访问，请检查网络连接或更换下载地址"
        exit 1
    fi

    # 下载 JDK 二进制安装包
    if ! wget --timeout=30 --no-check-certificate "${OPEN_JDK_URL}/${OPEN_JDK_FILE}" &>/dev/null; then
        error "Open JDK 安装包下载失败"
        rm -f "${OPEN_JDK_FILE}"
        exit 1
    fi

    success "Open JDK 安装包下载成功"
}

# 安装 JDK
install_jdk() {
    # 解压 JDK 二进制包
    if ! tar -xf "${SRC_DIR}/${OPEN_JDK_FILE}" -C "${INSTALL_DIR}"; then
        error "解压安装包失败"
        exit 1
    fi
    success "解压安装包成功"

    # 创建符号链接
    jdk_dir="${INSTALL_DIR}/jdk-${OPEN_JDK_VERSION}"
    if ! ln -snf "${jdk_dir}" "${OPEN_JDK_SYMLINK}"; then
        error "符号链接创建失败"
        exit 1
    fi
    success "符号链接创建完成"

    # 配置环境变量
    info "配置环境变量..."
    if [ ! -f "${OPEN_JDK_PROFILE}" ]; then
        if ! cat >"${OPEN_JDK_PROFILE}" <<EOF
export JAVA_HOME=${OPEN_JDK_SYMLINK}
export PATH=\${JAVA_HOME}/bin:\${PATH}
EOF
        then
            error "环境变量配置文件创建失败"
            exit 1
        fi

        if ! chmod +x "${OPEN_JDK_PROFILE}"; then
            error "设置环境变量文件权限失败"
            exit 1
        fi
    fi
    success "环境变量配置完成"

    # 立即生效环境变量
    if ! source "${OPEN_JDK_PROFILE}"; then
        error "环境变量生效失败"
        exit 1
    fi
    success "环境变量已生效"

    success "Oracle JDK 安装和配置已完成"
}

# 主函数
main() {
    error "========== 开始执行 Open JDK 安装脚本 =========="
    info "开始安装 Oracle JDK ${OPEN_JDK_VERSION}"
    check_root
    create_install_dir
    download_jdk
    install_jdk
    error "========== Open JDK 安装脚本执行完成 =========="
}

main "$@"
#!/bin/bash
#
# Maven 自动卸载脚本
# 作者: Grove Zhao
# 邮箱: grove.zhao@qq.com

# 严格模式
set -euo pipefail
IFS=$'\n\t'
trap 'error "脚本执行失败: 行号 $LINENO"' ERR

# 变量配置文件
source /root/script/config/variable_config.sh

# 日志和提示函数
log() {
    local msg=$1
    local color=$2
    local timestamp
    timestamp=$(date '+%Y-%m-%d %H:%M:%S')

    # 输出到控制台（带颜色）
    #echo -e "${color}${timestamp} ${msg}${PLAIN}"

    # 输出到日志文件（带颜色）
    echo -e "${color}${timestamp} ${msg}${PLAIN}" >> "$MAVEN_UNINSTALL_LOG"
}

# 简化的日志函数
error()   { log "$1" "$RED" >&2; }
warn()    { log "$1" "$YELLOW"; }
success() { log "$1" "$GREEN"; }
info()    { log "$1" "$BLUE"; }

# 检查运行权限
check_root() {
    info "检查运行权限..."
    if [ "$(id -u)" != "0" ]; then
        error "此脚本需要root权限运行"
        exit 1
    fi
    success "权限检查通过"
}

# 清理安装
cleanup_installation() {
    # 删除安装目录
    if [ -d "${INSTALL_DIR}/apache-maven-${MAVEN_VERSION}" ]; then
        info "正在删除安装目录: ${INSTALL_DIR}/apache-maven-${MAVEN_VERSION}"
        if rm -rf "${INSTALL_DIR}/apache-maven-${MAVEN_VERSION}"; then
            success "成功删除安装目录"
        else
            error "删除安装目录失败"
            return 1
        fi
    else
        warn "安装目录不存在: ${INSTALL_DIR}/apache-maven-${MAVEN_VERSION}"
    fi

    # 删除符号链接
    if [ -L "${MAVEN_SYMLINK}" ]; then
        info "正在删除符号链接: ${MAVEN_SYMLINK}"
        if rm -f "${MAVEN_SYMLINK}"; then
            success "成功删除符号链接"
        else
            error "删除符号链接失败"
            return 1
        fi
    else
        warn "符号链接不存在: ${MAVEN_SYMLINK}"
    fi

    # 删除环境变量配置
    if [ -f "$MAVEN_PROFILE" ]; then
        info "正在删除环境变量配置: $MAVEN_PROFILE"
        if rm -f "$MAVEN_PROFILE"; then
            success "成功删除环境变量配置"
            if source /etc/profile 2>/dev/null; then
                info "已重新加载环境变量配置"
            else
                warn "重新加载环境变量配置失败，请手动执行 source /etc/profile"
            fi
        else
            error "删除环境变量配置失败"
            return 1
        fi
    else
        warn "环境变量配置文件不存在: $MAVEN_PROFILE"
    fi

}

# 主函数
main() {
    error "========== 开始执行 Maven 卸载脚本 =========="
    check_root
    cleanup_installation
    error "==========  Maven 卸载脚本执行完成 =========="

}

# 执行主函数
main "$@"
#!/bin/bash
#
# Open JDK 自动安装脚本
# 作者: Grove Zhao
# 邮箱: grove.zhao@qq.com

# 严格模式
set -euo pipefail
IFS=$'\n\t'
trap 'error "脚本执行失败: 行号 $LINENO"' ERR

# 变量配置文件
source /root/script/config/variable_config.sh

# 日志和提示函数
log() {
    local msg=$1
    local color=$2
    local timestamp
    timestamp=$(date '+%Y-%m-%d %H:%M:%S')

    # 输出到控制台（带颜色）
    #echo -e "${color}${timestamp} ${msg}${PLAIN}"

    # 输出到日志文件（带颜色）
    echo -e "${color}${timestamp} ${msg}${PLAIN}" >> "$MAVEN_INSTALL_LOG"
}

# 简化的日志函数
error()   { log "$1" "$RED" >&2; }
warn()    { log "$1" "$YELLOW"; }
success() { log "$1" "$GREEN"; }
info()    { log "$1" "$BLUE"; }

# 检查运行权限
check_root() {
    if [ "$(id -u)" != "0" ]; then
        error "此脚本需要root权限运行"
        exit 1
    fi
    success "权限检查通过"
}

# 创建安装目录
create_install_dir() {
    if [ ! -d "${INSTALL_DIR}" ]; then
        mkdir -p "${INSTALL_DIR}" || {
            error "创建目录 ${INSTALL_DIR} 失败"
            exit 1
        }
        success "安装目录创建完成"
    fi
}

# 下载 Maven 二进制安装包
download_maven() {
    cd "${SRC_DIR}" || {
        error "无法进入目录 ${SRC_DIR}"
        exit 1
    }

    if [ -f "${MAVEN_FILE}" ]; then
        info "安装包已存在，跳过下载"
        return 0
    fi

    info "开始下载 Maven 二进制安装包"

    # 验证下载URL是否可访问
    if ! curl --connect-timeout 10 -sI "${MAVEN_URL}/${MAVEN_FILE}" &>/dev/null; then
        error "Maven 下载地址不可访问，请检查网络连接或更换下载地址"
        exit 1
    fi

    # 下载Maven 二进制安装包
    if ! wget --timeout=30 "${MAVEN_URL}/${MAVEN_FILE}" &>/dev/null; then
        error "Maven 二进制安装包下载失败"
        rm -f "${MAVEN_FILE}"
        exit 1
    fi

    success "Maven 二进制安装包下载成功"
}

# 安装 Maven
install_maven() {
    # 解压 Maven 二进制包    
    if ! tar -xf "${SRC_DIR}/${MAVEN_FILE}" -C "${INSTALL_DIR}"; then
        error "解压安装包失败"
        exit 1
    fi
    success "解压安装包成功"

   # 创建符号链接
    maven_dir="${INSTALL_DIR}/apache-maven-${MAVEN_VERSION}"
    if ! ln -snf "${maven_dir}" "${MAVEN_SYMLINK}"; then
        error "符号链接创建失败"
        exit 1
    fi
    success "符号链接创建完成"

    # 配置环境变量
    if [ ! -f "${MAVEN_PROFILE}" ]; then
        if ! cat >"${MAVEN_PROFILE}" <<EOF
export JAVA_HOME=${MAVEN_SYMLINK}
export PATH=\${JAVA_HOME}/bin:\${PATH}
EOF
        then
            error "环境变量配置文件创建失败"
            exit 1
        fi

        if ! chmod +x "${MAVEN_PROFILE}"; then
            error "设置环境变量文件权限失败"
            exit 1
        fi
    fi
    success "环境变量配置完成"

    # 立即生效环境变量
    if ! source "${MAVEN_PROFILE}"; then
        error "环境变量生效失败"
        exit 1
    fi
    success "环境变量已生效"

    success "Apache Maven 安装和环境变量配置已完成"
}

# 配置 Maven 镜像
configure_mirror() {
    # 备份原始配置文件
    if [ ! -f "${MAVEN_SETTINGS_FILE}.bak" ]; then
        cp "${MAVEN_SETTINGS_FILE}" "${MAVEN_SETTINGS_FILE}.bak" || {
            error "备份 Maven 配置文件失败"
            exit 1
        }
    fi

    # 配置阿里云镜像
    if grep -q "maven.aliyun.com" "${MAVEN_SETTINGS_FILE}"; then
        info "阿里云镜像已配置，跳过配置"
        return 0
    fi

    sed -i '/\/mirrors>/i \
    <mirror>\n \
      <id>nexus-aliyun</id>\n \
      <mirrorOf>*</mirrorOf>\n \
      <name>Nexus aliyun</name>\n \
      <url>https://maven.aliyun.com/repository/public</url>\n \
    </mirror>' "${MAVEN_SETTINGS_FILE}" || {
        error "配置 Maven 镜像失败"
        exit 1
    }

    success "Maven 镜像配置成功"
}

# 主函数
main() {
    error "========== 开始执行 Apache Maven 安装脚本 =========="
    info "开始安装 Apache Maven ${MAVEN_VERSION} "
    check_root
    create_install_dir
    download_maven
    install_maven
    configure_mirror
    error "========== Apache Maven 安装脚本执行完成 =========="
}

main "$@"